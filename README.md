# Presentations

This repository contains presentations on a range of subjects.

Rules:
1. The presentations are written in Latex, and a compiled pdf version is provided.
1. The presentations are not meant to be used as they are, but rather to serve as a prototype for the actual presentation.
1. The presentations must be able to stand on their own, meaning they must be understandable even if there's no speaker to explain them. It is assumed that if a speaker is to present them, the speaker should remove most of the written text from the files and instead say them out loud. In this case, the written text serves as annotation.
1. The presentations must be self-contained, not referring to other presentations.
1. A presentation can have one or more mini projects to serve as examples.
    * The mini projects must be working, using as little dependency as possible.
    * The mini projects must be portable, unless the presentation is specifically about an operating system.
1. Every presentation is inside a folder in the top level folder. No presentation allowed in the top level folder. No presentation allowed deeper than one folder. This rule is to guarantee the self-containment rule.s

Best practice:
1. The presentations should be short. 20 slides is probably the maximum. If there are too many slides, it should be divided into more presentations.
2. The title should be clear and should specify what technology is used (C++, Java, Git), unless it is very generalizable.