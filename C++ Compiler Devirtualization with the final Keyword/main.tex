\documentclass[10pt]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage[english,ngerman]{babel}

% ------------------------------------------------------------------------------
% Use the beautiful metropolis beamer template
% ------------------------------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{FiraSans} 
\mode<presentation>
{
  \usetheme[progressbar=foot,numbering=fraction,background=light]{metropolis} 
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
  %\setbeamertemplate{frame footer}{My custom footer}
} 

% ------------------------------------------------------------------------------
% beamer doesn't have texttt defined, but I usually want it anyway
% ------------------------------------------------------------------------------
\let\textttorig\texttt
\renewcommand<>{\texttt}[1]{%
  \only#2{\textttorig{#1}}%
}

% ------------------------------------------------------------------------------
% minted
% ------------------------------------------------------------------------------
\usepackage{minted}


% ------------------------------------------------------------------------------
% tcolorbox / tcblisting
% ------------------------------------------------------------------------------
\usepackage{xcolor}
\definecolor{codecolor}{HTML}{FFC300}

\usepackage{tcolorbox}
\tcbuselibrary{most,listingsutf8,minted}

\tcbset{tcbox width=auto,left=1mm,top=1mm,bottom=1mm,
right=1mm,boxsep=1mm,middle=1pt}

\newtcblisting{myr}[1]{colback=codecolor!5,colframe=codecolor!80!black,listing only, 
minted options={numbers=left, style=tcblatex,fontsize=\tiny,breaklines,autogobble,linenos,numbersep=3mm},
left=5mm,enhanced,
title=#1, fonttitle=\bfseries,
listing engine=minted,minted language=r}


% ------------------------------------------------------------------------------
% Listings
% ------------------------------------------------------------------------------
\definecolor{mygreen}{HTML}{37980D}
\definecolor{myblue}{HTML}{0D089F}
\definecolor{myred}{HTML}{98290D}

\usepackage{listings}

% the following is optional to configure custom highlighting
\lstdefinelanguage{XML}
{
  morestring=[b]",
  morecomment=[s]{<!--}{-->},
  morestring=[s]{>}{<},
  morekeywords={ref,xmlns,version,type,canonicalRef,metr,real,target}% list your attributes here
}

\lstdefinestyle{myxml}{
language=XML,
showspaces=false,
showtabs=false,
basicstyle=\ttfamily,
columns=fullflexible,
breaklines=true,
showstringspaces=false,
breakatwhitespace=true,
escapeinside={(*@}{@*)},
basicstyle=\color{mygreen}\ttfamily,%\footnotesize,
stringstyle=\color{myred},
commentstyle=\color{myblue}\upshape,
keywordstyle=\color{myblue}\bfseries,
}

% ------------------------------------------------------------------------------
% The Document
% ------------------------------------------------------------------------------
\title{Compiler devirtualization with the 'final' keyword}
\author{Luiz Chagas Jardim}
\date{March 2020}
\usemintedstyle[c++]{}
\newcommand{\cpp}{C++}

\begin{document}

\maketitle

\section{The Problem}
\begin{frame}[fragile]{The Set Up}
It's a common pattern to expose interfaces and hide implementations.
\pause

One way this is done in \cpp is through inheritance of abstract classes.
\pause

The client accesses an abstract class pointer that points to an instance of the implementation.
\end{frame}

\begin{frame}[fragile]{Example}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
//public_header.h
//distributed with the library
struct Class {
    static std::unique_ptr<Class> create();
    virtual void foobar() = 0;
    virtual void bar() = 0;
};
\end{minted}
\pause
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
//private_header.h
//not distributed
struct ClassImpl : Class {
    void foobar() override;
    void bar() override;
};
\end{minted}
\end{frame}

\begin{frame}[fragile]{Example (cont.)}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
//private_header.cpp
std::unique_ptr<Class> Class::create() {
    return std::make_unique<ClassImpl>();
}
void ClassImpl::foobar() {
    std::cout << "foo";
    bar();
}
void ClassImpl::bar() {
    std::cout << "bar\n";
}
\end{minted}
\pause
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
//client_code.cpp
auto instance = Class::create();
instance->foobar();
\end{minted}
\end{frame}

\begin{frame}[fragile]{Explanation}
When the client calls \lstinline{Class::create()}, it allocates an instance of \lstinline{ClassImpl} on the heap, which includes a vtable for the virtual member functions \lstinline{foobar()} and \lstinline{bar()}.
\pause

When the client calls \lstinline{instance->foobar()}, the program goes to the vtable of \lstinline{instance}, which then points to \lstinline{ClassImpl::foobar()}.
\pause

Then after foo is printed, it must call \lstinline{instance->bar()}.
\pause

The program goes to the vtable again to find \lstinline{ClassImpl::bar()} and prints bar.
\end{frame}

\begin{frame}[fragile]{Optimization}
The compiler may be able to optmize away some of those vtable calls.
\pause

With the flag -O3, the compiler can predict the call to \lstinline{ClassImpl::foobar()} and not generate assembly for it.\footnote{\url{https://godbolt.org/z/KGFJpu}}
\pause

But the compiler cannot predict the call to bar.
\pause

For example, a client might extend from \lstinline{ClassImpl} (the compiler doesn't know that that header is private) and override \lstinline{bar}, but not \lstinline{foobar}.
\pause

With a big code base with a lot of these cases, the performance may be affected.
\end{frame}

\section{The Solution}

\begin{frame}[fragile]{The 'final' keyword}
With the 'final' keyword in a class, the class cannot be extended.
\pause
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct FinalClass final {};
struct ImpossibleClass : FinalClass {}; //Compilation error
\end{minted}
\pause
Now the compiler can make some optimizations based on this fact and de-virtualize some member functions.
\pause

Applying it to our earlier example, we can see the compiler does not generate assembly for bar anymore.\footnote{\url{https://godbolt.org/z/Ckm4_I}}
\end{frame}

\begin{frame}[fragile]{When to use 'final'}
Whenever your class won't be extended. Even if a class has to be extendable, member functions can be marked final when it doesn't make sense to override them.
\pause

As a bonus, any client or maintainer knows that member function will not be overridden.
\pause

Consider this contrived example:\footnote{\url{https://godbolt.org/z/oofwo5}}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct Interface /*can't be final*/ {
    virtual int getX() = 0;
    virtual void setX(int) = 0;
    virtual void increaseX() {
        setX(getX()+1);
    };
};
//(cont.)
\end{minted}
\end{frame}

\begin{frame}[fragile]{When to use 'final' (cont.)}

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct Implementation /*can't be final*/ : Interface {
    int getX() /*override or final?*/ {
        return x;
    }
    void setX(int value) /*override or final?*/ {
        x = value;
    }
private:
    int x = 0;
};
\end{minted}
\pause
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
struct Derived /*final?*/ : Implementation {
    void increaseX() /*override or final*/ {
        setX(getX()+2);
    }
};
\end{minted}

\end{frame}

\begin{frame}[fragile]{When to use 'final' (cont.)}
In this example the Interface class and the Implementation class cannot be final, because they are extended by other classes. Maybe the Derived class can.
\pause

But does it make sense for a class inheriting from the Implementation class to override getX() or setX()? If not, then they can be marked as final member functions.

\end{frame}

\begin{frame}[standout]
    Thank you ~\alert{\faSmileO}~
\end{frame}
\end{document}
