\documentclass[10pt]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage[english,ngerman]{babel}

% ------------------------------------------------------------------------------
% Use the beautiful metropolis beamer template
% ------------------------------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{FiraSans} 
\mode<presentation>
{
  \usetheme[progressbar=foot,numbering=fraction,background=light]{metropolis} 
  \usecolortheme{crane} % or try albatross, beaver, crane, ...
  \usefonttheme{structurebold}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
  %\setbeamertemplate{frame footer}{My custom footer}
} 

% ------------------------------------------------------------------------------
% beamer doesn't have texttt defined, but I usually want it anyway
% ------------------------------------------------------------------------------
\let\textttorig\texttt
\renewcommand<>{\texttt}[1]{%
  \only#2{\textttorig{#1}}%
}

% ------------------------------------------------------------------------------
% minted
% ------------------------------------------------------------------------------
\usepackage{minted}


% ------------------------------------------------------------------------------
% tcolorbox / tcblisting
% ------------------------------------------------------------------------------
\usepackage{xcolor}
\definecolor{codecolor}{HTML}{FFC300}

\usepackage{tcolorbox}
\tcbuselibrary{most,listingsutf8,minted}

\tcbset{tcbox width=auto,left=1mm,top=1mm,bottom=1mm,
right=1mm,boxsep=1mm,middle=1pt}

\newtcblisting{myr}[1]{colback=codecolor!5,colframe=codecolor!80!black,listing only, 
minted options={numbers=left, style=tcblatex,fontsize=\tiny,breaklines,autogobble,linenos,numbersep=3mm},
left=5mm,enhanced,
title=#1, fonttitle=\bfseries,
listing engine=minted,minted language=r}


% ------------------------------------------------------------------------------
% Listings
% ------------------------------------------------------------------------------
\definecolor{mygreen}{HTML}{37980D}
\definecolor{myblue}{HTML}{0D089F}
\definecolor{myred}{HTML}{98290D}

\usepackage{listings}

% the following is optional to configure custom highlighting
\lstdefinelanguage{XML}
{
  morestring=[b]",
  morecomment=[s]{<!--}{-->},
  morestring=[s]{>}{<},
  morekeywords={ref,xmlns,version,type,canonicalRef,metr,real,target}% list your attributes here
}

\lstdefinestyle{myxml}{
language=XML,
showspaces=false,
showtabs=false,
basicstyle=\ttfamily,
columns=fullflexible,
breaklines=true,
showstringspaces=false,
breakatwhitespace=true,
escapeinside={(*@}{@*)},
basicstyle=\color{mygreen}\ttfamily,%\footnotesize,
stringstyle=\color{myred},
commentstyle=\color{myblue}\upshape,
keywordstyle=\color{myblue}\bfseries,
}

% ------------------------------------------------------------------------------
% The Document
% ------------------------------------------------------------------------------
\title{Refactoring Techniques - Composing Methods}
\author{Jorge Pinto Sousa}
\date{May 2020}
\usemintedstyle[c++]{}
\newcommand{\cpp}{C++}

\begin{document}

\maketitle

\section{Introduction}
\begin{frame}[fragile]{Composing Methods}
 \begin{itemize}
    \item  The techniques included in this group are essentially devoted to the craft of correctly composing methods.
    \item  How many times do we felt the pain of getting our hands around very long methods ?

    \item These excessively extensive and long methods usually obscure execution logic and make the method extremely hard to understand and consequently, even harder to change.
\end{itemize}
\end{frame}
\section{The different techniques}
\begin{frame}[fragile]{Extract Method}
\textbf{Problem}

There's a piece of code that can be grouped together:

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
void printTrade() 
{
    printId();

    // trade's envelope details
    std::cout << getEnvExtRef() << '\n';
    std::cout << getEnvCust()  << '\n';
}
\end{minted}
\end{frame}
\begin{frame}[fragile]{Extract Method}
\textbf{Solution}
\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
void printTrade() 
{
    printId();
    printEnvelope();
}

void printEnvelope()
{
    std::cout << getEnvExtRef() << '\n';
    std::cout << getEnvCust()  << '\n';
}
\end{minted}
\end{frame}
\begin{frame}[fragile]{Why Extract Method?}
\begin{itemize}
    \item Improves readability showing intention.
    \begin{minted}
        [frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize]
        {c++}
        //This 
            std::cout << env->Back->Ext->ExtRef << '\n';
        //Vs
            getExtRef();
    \end{minted}
    \item Reduces code duplication. Most often the code can be seen and reused in another locations on the program.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Inline Method}
\textbf{Problem}

When the body of a method is way more obvious than the method name.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class TradeNotional {
  // ...
  int getNotionalRating() const{
    return isNotionalBiggerThan5Million() ? 2 : 1;
  }
  bool isNotionalBiggerThan5Million() const{
    return notional > 5000000;
  }
};
\end{minted}
\end{frame}

\begin{frame}[fragile]{Inline Method}
\textbf{Solution}

Replace the method call with the method's content.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class TradeNotional {
  // ...
  int getNotionalRating() const{
    return notional > 5000000 ? 2 : 1;
  }
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Inline Method?}
\begin{itemize}
    \item Improves readability making code more straightforward.
    \item Many times these methods create a confusing maze.
    \item Many times these methods were not so short at first but were \textit{eroded} over time.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Extract Variable}
\textbf{Problem}

An expression that is very difficult to understand.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}

void renderBanner() 
{
  if(platform.compare("linux") == 0 && browser.compare("mozilla") == 0 
                                    && resize > 0 && isInitialized()){
       //do something
    }
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Extract Variable}
\textbf{Solution}

Assign the result of the expression to variable(s) with self-explanatory names.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
void renderBanner() 
{
  const bool isLinux = platform.compare("linux") == 0;
  const bool isMozilla = browser.compare("mozilla") == 0;
  const bool wasResized = resize > 0;

  if (isLinux && isMozilla && wasInitialized() && wasResized) {
    // do something
  }
\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Extract Variable?}
\begin{itemize}
    \item Readability again! Try to give the extracted variables good names that show its purpose and intent.
    \item Make a complex expression easier to understand.
    \item These can be: long arithmetic expressions, long if conditions...
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Inline Temp}
\textbf{Problem}

You have a temporary variable that is assigned the result of a simple expression and nothing more.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
bool hasDiscount(const Customer& cust) {
  std::string custClass = cust.getClassficiation();
  return custClass.compare("AAA") == 0;
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Inline Temp}
\textbf{Solution}

Replace the references to the variable with the expression itself.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
bool hasDiscount(const Customer& cust) {
  return cust.getClassficiation().compare("AAA") == 0;
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Inline Temp?}
\begin{itemize}
    \item You get rid of an unnecessary variable :) .
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Replace Temp with Query}
\textbf{Problem}

You put the result of a computation a local variable.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
double calculateTotal() {
  double baseValue = notional * bondPrice;
  if (baseValue > 1000) {
    return baseValue * 0.95;
  }
  else {
    return baseValue * 0.98;
  }
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Replace Temp with Query}
\textbf{Solution}

Move the computation to a separate method and instead of assigning the computation to a local variable, query it.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
double calculateTotal() {
  if (baseValue() > 1000) {
    return baseValue() * 0.95;
  }
  else {
    return baseValue() * 0.98;
  }
}
double baseValue() {
  return notional * bondPrice;
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Replace Temp with Query?}
\begin{itemize}
    \item Readability, Readability, Readability.
    \item Maybe the computation is duplicated along several other methods.
\end{itemize}
\textbf{If the expression is really hard to calculate, and very time-consuming, one should not use this refactoring technique}
\end{frame}

\begin{frame}[fragile]{Split Temporary Variable}
\textbf{Problem}

A local variable that is used to store various different values inside a method.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
double tmp = 2 * (height + width);
std::cout << tmp << '\n';
tmp = height * width;
std::cout << tmp << '\n';
\end{minted}
\end{frame}

\begin{frame}[fragile]{Split Temporary Variable}
\textbf{Solution}

Different variables for different values. 

Each variable should be responsible for one and only one thing.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
const double perimeter = 2 * (height + width);
std::cout << perimeter << '\n';
const double area = height * width;
std::cout << area << '\n';
\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Split Temporary Variable?}
\begin{itemize}
    \item Readability again. Lot of the times this type of variables are named:
    temp, tmp, val, etc...
    \item Each component of the code should be responsible for one thing only. Respecting this highly increases maintainability, since we are less prone to have unwanted side effects.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Replace Method with Method Object}
\textbf{Problem}

You have a long method and the local variables and the logic is so entangled that you cannot apply the extract method technique.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class Trade {
  // ...
  public: 
  double price() {
    double primaryBasePrice;
    double secondaryBasePrice;
    double tertiaryBasePrice;
    // Perform long computation.
  }
};
\end{minted}
\end{frame}

\begin{frame}[fragile]{Replace Method with Method Object}
\textbf{Solution}

Transform the method into a separate class so that the local variables become fields of the class, and then split the method into several submethods.


\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
class Trade {
  // ...
public: 
  double price() {
    return PriceCalculator(this).compute();
  }
};

class PriceCalculator {
private: 
  double primaryBasePrice;
  double secondaryBasePrice;
  double tertiaryBasePrice;

public: 

  public double compute() {
    // Perform long computation.
  }
};

\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Replace Method with Method Object?}
\begin{itemize}
    \item By improving readability and better exposing the logic of the method's code, this action can help to stop the increase its size.
    \item Opens the way for splitting a large and confusing method into smaller ones.
    \item Does not polute the original class with utility methods.
\end{itemize}
\textbf{However this implies adding another class, and that means to increase the code complexity.}
\end{frame}

\begin{frame}[fragile]{Substitute Algorithm}
\textbf{Problem}

You have a method with a lot of issues, maybe very entangled and maybe you also found a better way to achieve the same result.

\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
std::string foundPerson(const std::vector<std::string>& people)
{
  for (size_t i = 0; i < people.size(); i++) {
    if (people[i].compare("Any") == 0){
      return "Any";
    }
    if (people[i].compare("Body") == 0){
      return "Body";
    }
    if (people[i].compare("JDOE") == 0){
      return "JDOE";
    }
  }
  return "";
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Substitute Algorithm}
\textbf{Solution}

Just do it!


\begin{minted}
[frame=none,framesep=2mm,baselinestretch=0,bgcolor=white,fontsize=\footnotesize,linenos]
{c++}
std::string foundPerson(const std::vector<std::string>& people)
{
    std::vector<std::string> candidates{"Any", "Body", "JDOE"};
    for(size_t i = 0; i < people.size(); ++i){
        if(std::find(candidates.begin(), candidates.end(), people[i]) 
                                        != candidates.end()){
            return people[i];
        }
    }
  return "";
}

\end{minted}
\end{frame}

\begin{frame}[fragile]{Why Substitute Algorithm?}
\begin{itemize}
    \item Sometimes gradual refactoring is not possible or very difficult.
    \item We might be implementing ourselves something that has now been incorporated in some well-know 3rd party library or even in the standard.
\end{itemize}
\end{frame}

\begin{frame}[standout]
    Thank you ~\alert{\faSmileO}~
\end{frame}
\end{document}
